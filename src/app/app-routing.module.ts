import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CurrentComponent } from './current/current.component';
import { FivedayComponent } from './fiveday/fiveday.component';
import { NavigationComponent } from './navigation/navigation.component';


const routes: Routes = [
  { path: '', component: CurrentComponent },
  { path: 'fiveday', component: FivedayComponent },
  { path: '', redirectTo: '', pathMatch: 'full' },
  //{ path: '**', redirectTo: '' }
  { path: '**', component: CurrentComponent }




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }