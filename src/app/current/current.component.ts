import { Component, OnInit } from '@angular/core';
import { Current } from '../current';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-current',
  templateUrl: './current.component.html',
  styleUrls: ['./current.component.css']
})

export class CurrentComponent implements OnInit {

  weather: Current;
  location: any = {};
  exist = 0;
  appId = 'aff492c9ea59bd7924a4e85dfb61458c';
  baseUrl = 'http://api.openweathermap.org/data/2.5/';
  units = 'metric';
  lang = 'pl';
  constructor(private http: HttpClient) { }

  ngOnInit() {
    return new Promise((res, rej) => {
      navigator.geolocation.getCurrentPosition((pos) => {
        this.location = pos.coords;
        const lat = this.location.latitude;
        const lon = this.location.longitude;
        console.log(`lat ${lat} and lon ${lon}`);
        return this.http.get<Sidebar>(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=aff492c9ea59bd7924a4e85dfb61458c&units=metric&lang=pl`)
          .toPromise().then(
          (data) => {
            this.weather = new Current(
              data.name,
              data.main.temp,
              data.weather[0].description,
              data.main.temp_min,
              data.main.temp_max,
              data.weather[0].icon);
            res(this.weather);
            this.exist++;
            return this.weather;

          }
          );
      });
    });
  }
  onSubmit(weatherForm: NgForm) {
    this.exist++;
    return this.http.get<Sidebar>(this.baseUrl + `weather?q=${weatherForm.value.city}&appid=` + this.appId + `&units=` + this.units + `&lang=` + this.lang)
    .subscribe(
      (data) => {
        this.weather = new Current(
          data.name, data.main.temp,
          data.weather[0].description,
          data.main.temp_min,
          data.main.temp_max,
          data.weather[0].icon);
      }
    );
  }

}
interface Sidebar {
  name: any;
  main: any;
  weather: any;
}
