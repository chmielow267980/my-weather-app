import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Fiveday } from '../fiveday';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-fiveday',
  templateUrl: './fiveday.component.html',
  styleUrls: ['./fiveday.component.css']
})
export class FivedayComponent implements OnInit {
  appId = 'aff492c9ea59bd7924a4e85dfb61458c';
  baseUrl = 'http://api.openweathermap.org/data/2.5/';
  units = 'metric';
  lang = 'pl';
  constructor(private http: HttpClient) { }

  forecastForm: FormGroup;
  forecast: Fiveday[] = [];
  ngOnInit() {
    this.forecastForm = new FormGroup({
      forecastCity: new FormControl('')
    });
  }
  onSubmit() {
    this.forecast.splice(0, this.forecast.length);
    return this.http.get<Sidebar>(this.baseUrl + `forecast?q=${this.forecastForm.value.forecastCity}&appid=` + this.appId + `&units=` + this.units + `&lang=` + this.lang)
    .subscribe(
      (data) => {
        for (let i = 0; i < data.list.length; i = i + 8) {
          const forecastWeather = new Fiveday(
            data.city.name,
            data.list[i].weather[0].description,
            data.list[i].main.temp,
            data.list[i].dt_txt,
            data.list[i].weather[0].icon);
          this.forecast.push(forecastWeather);
        }
        return this.forecast;
      }
    );
  }

}

interface Sidebar {
  city: any;
  list: any;
}
